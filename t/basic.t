#! perl

use Test2::V0;
use Test::Alien::CPP;
use CXC::Alien::XTime;

alien_ok 'CXC::Alien::XTime';

# if it's a "share" build, the timing database is in the build
# directory and hasn't been installed yet, so need to point at it
if ( CXC::Alien::XTime->install_type eq 'share' ) {
    $ENV{TIMING_DIR} = join( '/', CXC::Alien::XTime->dist_dir, 'share', 'XTime' );
}

xs_ok do { local $/; <DATA> }, with_subtest {
    is( Chandra::Time::convert_time( '1998-12-14T05:18:56.816', 't', 'f3', 'u', 's' ),
        float('29999936.815999999642372131'), 'XTime::convert() returns 1' );
};

done_testing;

__DATA__
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <XTime.h>

MODULE = Chandra::Time          PACKAGE = Chandra::Time
char *
convert_time( time_in, ts_in, tf_in, ts_out, tf_out )
  char *time_in
  char *ts_in
  char *tf_in
  char *ts_out
  char *tf_out
 CODE:
  New( 0, RETVAL, 255, char );
  axTime3( time_in, ts_in, tf_in, ts_out, tf_out, RETVAL);
 OUTPUT:
  RETVAL
 CLEANUP:
  Safefree( RETVAL );
